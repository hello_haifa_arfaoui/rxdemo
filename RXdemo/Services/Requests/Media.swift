//
//  Media.swift
//  RXdemo
//
//  Created by Haifa Arfaoui on 9/16/20.
//  Copyright © 2020 Haifa Arfaoui. All rights reserved.
//

import Foundation

struct Media{
    static let movie = "movie"
    static let podcast = "podcast"
    static let music = "music"
    static let musicVideo = "musicVideo"
    static let audiobook = "audiobook"
    static let shortFilm = "shortFilm"
    static let tvShow = "tvShow"
    static let software = "software"
    static let ebook = "ebook"
    static let all = "all"
    
}
