//
//  Track.swift
//  RXdemo
//
//  Created by Haifa Arfaoui on 9/15/20.
//  Copyright © 2020 Haifa Arfaoui. All rights reserved.
//

import Foundation

// MARK: - Tracks
struct Tracks: Codable {
    let resultCount: Int
    let results: [Track]
}

// MARK: - Track
struct Track: Codable {
    let wrapperType: WrapperType?
    let kind: Kind?
    let collectionID, trackID: Int?
    let artistName, collectionName, trackName, collectionCensoredName: String?
    let trackCensoredName: String?
    let collectionArtistID: Int?
    let collectionArtistViewURL, collectionViewURL, trackViewURL: String?
    let previewURL: String?
    let artworkUrl30, artworkUrl60, artworkUrl100: String?
    let collectionPrice, trackPrice, trackRentalPrice, collectionHDPrice: Double?
    let trackHDPrice, trackHDRentalPrice: Double?
    let releaseDate: String?
    let collectionExplicitness, trackExplicitness: Explicitness?
    let discCount, discNumber, trackCount, trackNumber: Int?
    let trackTimeMillis: Int?
    let country: Country?
    let currency: Currency?
    let primaryGenreName, contentAdvisoryRating, longDescription: String?
    let hasITunesExtras: Bool?
    let shortDescription: String?
    let artistID: Int?
    let artistViewURL: String?
    let isStreamable: Bool?
    let resultDescription, copyright, collectionArtistName: String?
    let amgArtistID: Int?

    enum CodingKeys: String, CodingKey {
        case wrapperType, kind
        case collectionID
        case trackID
        case artistName, collectionName, trackName, collectionCensoredName, trackCensoredName
        case collectionArtistID
        case collectionArtistViewURL
        case collectionViewURL
        case trackViewURL
        case previewURL
        case artworkUrl30, artworkUrl60, artworkUrl100, collectionPrice, trackPrice, trackRentalPrice
        case collectionHDPrice
        case trackHDPrice
        case trackHDRentalPrice
        case releaseDate, collectionExplicitness, trackExplicitness, discCount, discNumber, trackCount, trackNumber, trackTimeMillis, country, currency, primaryGenreName, contentAdvisoryRating, longDescription, hasITunesExtras, shortDescription
        case artistID
        case artistViewURL
        case isStreamable
        case resultDescription
        case copyright, collectionArtistName
        case amgArtistID
    }
}

enum Explicitness: String, Codable {
    case explicit = "explicit"
    case notExplicit = "notExplicit"
}

enum Country: String, Codable {
    case usa = "USA"
}

enum Currency: String, Codable {
    case usd = "USD"
}

enum Kind: String, Codable {
    case featureMovie = "feature-movie"
    case song = "song"
}

enum WrapperType: String, Codable {
    case audiobook = "audiobook"
    case track = "track"
}
