//
//  API.swift
//  RXdemo
//
//  Created by Haifa Arfaoui on 9/15/20.
//  Copyright © 2020 Haifa Arfaoui. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class API{
    /// The URL string
     func urlString(searchText: String, queryComponents: [URLQueryItem] ) -> URL {
        var queryComponents = queryComponents
        var urlComponents = URLComponents(string: Urls.searchUrl)
       let encodedText = searchText.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        if !searchText.isEmpty{
            queryComponents.insert(URLQueryItem(name: "term", value: encodedText), at: 0)
        }
        urlComponents?.queryItems = queryComponents
        let url = urlComponents!.url
       return url!
     }

     /// Parse JSON data
     func parse(data: Data) -> [Track] {
       do {
        if let res = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any] {
            print(res)
        }
           let result = try JSONDecoder().decode(Tracks.self, from:data)
           return result.results
       } catch {
           print("Error: \(error)")
           return []
       }
     }

     
}

class ApiSearch: API  {
    
  static var shared = ApiSearch()

 /// Make API request
    func search(search term: String, mediaType: String) -> Observable<[Track]> {
    let url = urlString(searchText: term, queryComponents: [URLQueryItem(name: "media", value: mediaType)])
    let request = URLRequest(url: url)
    let session = URLSession.shared
    return session.rx.data(request: request).map { (self.parse(data: $0)) }
  }
}
