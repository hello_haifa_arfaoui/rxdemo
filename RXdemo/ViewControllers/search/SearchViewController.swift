//
//  SearchViewController.swift
//  RXdemo
//
//  Created by Haifa Arfaoui on 9/15/20.
//  Copyright © 2020 Haifa Arfaoui. All rights reserved.
//

import UIKit
import RxSwift

class SearchViewController: UIViewController {

    
    @IBOutlet var favoriteArtistOrPodcastSegmentControl: UISegmentedControl!{
        didSet{
            favoriteArtistOrPodcastSegmentControl.addTarget(self, action: #selector(selectedSegment), for: .valueChanged)
        }
    }
    
    @IBOutlet var searchForFavoriteArtistView: UIView!
    
    @IBOutlet var favoriteArtistSearchBar: UISearchBar!{
        didSet{
            let searchBarStyle = favoriteArtistSearchBar.value(forKey: "searchField") as? UITextField
            searchBarStyle?.clearButtonMode = .never
        }
    }
    
    @IBOutlet var favoritArtistTableView: UITableView!
    
    @IBOutlet var searchForMoviesView: UIView!
    
    @IBOutlet var moviesSearchBar: UISearchBar!{
        didSet{
            let searchBarStyle = moviesSearchBar.value(forKey: "searchField") as? UITextField
            searchBarStyle?.clearButtonMode = .never
        }
    }
    
    @IBOutlet var moviesTableView: UITableView!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
           super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
       }
       
       required init?(coder: NSCoder) {
           fatalError("init(coder:) has not been implemented")
       }

    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        favoritArtistTableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "TableViewCell")
        moviesTableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "TableViewCell")
        selectedSegment()
    }


    @objc func selectedSegment(){
        if favoriteArtistOrPodcastSegmentControl.selectedSegmentIndex == 0{
            displayFavoriteArtistSearchView()
        }else if favoriteArtistOrPodcastSegmentControl.selectedSegmentIndex == 1{
            displayMovieSearchView()
        }
    }

    func displayFavoriteArtistSearchView(){
        view.bringSubviewToFront(searchForFavoriteArtistView)
        rxSetup(tableView: favoritArtistTableView, searchField: favoriteArtistSearchBar, mediaType: Media.all)
    }
    
    func displayMovieSearchView(){
        view.bringSubviewToFront(searchForMoviesView)
        rxSetup(tableView: moviesTableView, searchField: moviesSearchBar, mediaType: Media.movie)
    }
    
    func rxSetup(tableView:UITableView, searchField:UISearchBar, mediaType: String) {
        let results = searchField.rx.text.orEmpty
            .throttle(.milliseconds(3), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .flatMapLatest { query -> Observable<[Track]> in
                if query.isEmpty {
                    return .just(Tracks(resultCount: 0, results: []).results)
                }
                return ApiSearch.shared.search(search: query, mediaType: mediaType)
                    .catchErrorJustReturn(Tracks(resultCount: 0, results: []).results)
        }
        .observeOn(MainScheduler.instance)
        
        results
            .bind(to: tableView.rx.items(cellIdentifier: "TableViewCell",
                                               cellType: TableViewCell.self)) {
                                                (index, track: Track, cell) in
                                                cell.setup(for: track)
        }
        .disposed(by: disposeBag)
        
    }
    
}
