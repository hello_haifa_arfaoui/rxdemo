//
//  TableViewCell.swift
//  RXdemo
//
//  Created by Haifa Arfaoui on 9/15/20.
//  Copyright © 2020 Haifa Arfaoui. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet var trackImage: UIImageView!
    @IBOutlet var trackTitleLabel: UILabel!
    @IBOutlet var trackSubtitleLabel: UILabel!
    @IBOutlet var trackPriceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(for track: Track){
        if track.artworkUrl100 != nil{
            trackImage.setImage(url: URL(string:track.artworkUrl100!)!, placeholder: .none)
        }
        if track.collectionName != nil{
            trackTitleLabel.text = track.collectionName!
        }
        if track.artistName != nil{
            trackSubtitleLabel.text = track.artistName!
        }
        if track.trackPrice != nil{
            trackPriceLabel.text = "\(track.trackPrice!)"
        }
        
    }
}
