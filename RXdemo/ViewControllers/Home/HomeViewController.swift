//
//  HomeViewController.swift
//  RXdemo
//
//  Created by Haifa Arfaoui on 9/15/20.
//  Copyright © 2020 Haifa Arfaoui. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    
    @IBOutlet var topSongsView: TracksSwipingView!{
        didSet{
            topSongsView.backgroundColor = .clear
            topSongsView.title.text  = Constants.topSongs
        }
    }
    
    @IBOutlet var podcastsView: TracksSwipingView!{
        didSet{
            podcastsView.backgroundColor = .clear
            podcastsView.title.text = Constants.podcasts
        }
    }
    
    @IBOutlet var moviesView: TracksSwipingView!{
        didSet{
            moviesView.backgroundColor = .clear
            moviesView.title.text = Constants.movies
        }
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
              super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
          }
          
          required init?(coder: NSCoder) {
              fatalError("init(coder:) has not been implemented")
          }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
