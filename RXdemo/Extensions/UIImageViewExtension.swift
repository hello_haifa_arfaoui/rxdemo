//
//  UIImageViewExtension.swift
//  RXdemo
//
//  Created by Haifa Arfaoui on 9/16/20.
//  Copyright © 2020 Haifa Arfaoui. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

extension UIImageView{
    
    func setImage(url:URL, placeholder:UIImage?){
        sd_setImage(with: url, placeholderImage: placeholder)
    }
    
}
