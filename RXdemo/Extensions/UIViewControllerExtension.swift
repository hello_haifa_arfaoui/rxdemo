//
//  UIViewControllerExtension.swift
//  RXdemo
//
//  Created by Haifa Arfaoui on 9/16/20.
//  Copyright © 2020 Haifa Arfaoui. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
  
    func popAlert (title: String, message: String, handler: ((UIAlertAction) -> Void)?){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "ok", style: .cancel, handler: handler)
        alertController.addAction(defaultAction)
        present(alertController, animated: true, completion: nil)
    }

}
