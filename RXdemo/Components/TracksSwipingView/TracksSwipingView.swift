//
//  TracksSwipingView.swift
//  RXdemo
//
//  Created by Haifa Arfaoui on 9/15/20.
//  Copyright © 2020 Haifa Arfaoui. All rights reserved.
//

import Foundation
import UIKit

protocol TracksSwipingViewDelegate : AnyObject {
    func goToTrackDetail(track:Track)
}

@IBDesignable
class TracksSwipingView: UIView {
    
    //MARK: - Constants
    
    private struct Metrics {
        static let flowLayoutSpacing : CGFloat = 4
        static let leadingSpace : CGFloat  = 10
        static let trailingSpace  : CGFloat = -10
        static let topSpace : CGFloat = 15
        static let bottomSpace : CGFloat  = 10
        static let titleHeight : CGFloat  = 20
        static let textSize : CGFloat = 18
        static let cellWidth : CGFloat = 137
    }
    
    //MARK: - Properties
    var tracks : Tracks = Tracks (resultCount: 0, results: [])
    
    //MARK: - Delegates
    
    weak var delegate : TracksSwipingViewDelegate?
    //MARK: - Outlets
    
    public lazy var title : UILabel = {
        let title : UILabel = UILabel(frame: .zero)
        title.numberOfLines = 1
        title.lineBreakMode = .byWordWrapping
        title.translatesAutoresizingMaskIntoConstraints = false
        title.textColor = .black
        title.font = UIFont.boldSystemFont(ofSize: 16)
        title.clipsToBounds = false
        title.layer.masksToBounds = true
        return title
    }()

    private lazy var collectionViewLayout: UICollectionViewFlowLayout = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = Metrics.flowLayoutSpacing
        flowLayout.minimumInteritemSpacing = Metrics.flowLayoutSpacing
        return flowLayout
    }()
    
    public lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
        collectionView.clipsToBounds = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isPagingEnabled = true
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        collectionView.register(UINib(nibName: "TrackCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TrackCollectionViewCell")
        return collectionView
    }()
    
    //MARK: - Initializers
    
    init() {
        super.init(frame: .zero)
        constraintUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        constraintUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        constraintUI()
    }
    
    //MARK: - Constraints
        
    func constraintUI() {
        
        addSubview(title)
        addSubview(collectionView)
        
        NSLayoutConstraint.activate([
            
            title.topAnchor.constraint(equalTo: self.topAnchor),
            title.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Metrics.leadingSpace),
            title.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            title.heightAnchor.constraint(equalToConstant: Metrics.titleHeight),

            collectionView.topAnchor.constraint(equalTo: title.bottomAnchor, constant: Metrics.bottomSpace),
            collectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Metrics.leadingSpace),
            collectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: Metrics.trailingSpace),
            collectionView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
    
    func configure(tracks: Tracks) {
        self.tracks = tracks
        collectionView.reloadData()
    }

}
extension TracksSwipingView: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tracks.resultCount
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrackCollectionViewCell", for: indexPath) as? TrackCollectionViewCell
            else { return UICollectionViewCell() }
        cell.layer.cornerRadius = 5
        cell.clipsToBounds = true
        cell.event = tracks.results[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.goToTrackDetail(track: tracks.results[indexPath.row])
    }
    
}

extension TracksSwipingView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: Metrics.cellWidth, height: collectionView.bounds.size.height - Metrics.titleHeight)
    }
}
