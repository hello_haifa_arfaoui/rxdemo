//
//  AppDelegate.swift
//  RXdemo
//
//  Created by Haifa Arfaoui on 9/10/20.
//  Copyright © 2020 Haifa Arfaoui. All rights reserved.
//

import Foundation
import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        startApp()
        return true
    }
    
    func startApp(){
        window = UIWindow(frame: UIScreen.main.bounds)
        let tabBar = UITabBarController()
        let rootViewControllerOne = UINavigationController(rootViewController: HomeViewController.init(nibName: "HomeViewController", bundle: nil) )
        rootViewControllerOne.viewControllers.first?.title = Constants.home
        rootViewControllerOne.tabBarItem = UITabBarItem(title: Constants.home, image: .icHome, tag: 0)
        let rootViewControllerTwo = UINavigationController(rootViewController: SearchViewController.init(nibName: "SearchViewController", bundle: nil) )
        rootViewControllerTwo.viewControllers.first?.title = Constants.search
        rootViewControllerTwo.tabBarItem = UITabBarItem(title: Constants.search, image: .icSearch, tag: 1)
        tabBar.setViewControllers( [rootViewControllerOne, rootViewControllerTwo], animated: true)
        window?.rootViewController = tabBar
        window?.makeKeyAndVisible()
    }

}
