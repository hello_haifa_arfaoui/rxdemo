//
//  UIColorExtensions.swift
//  RXdemo
//
//  Created by Haifa Arfaoui on 9/15/20.
//  Copyright © 2020 Haifa Arfaoui. All rights reserved.
//

import Foundation
import UIKit

fileprivate class AmuseeColors { }

extension UIColor {
        
    /// A color object whose hex value is #D948DD and whose alpha value is 1.0.
    public class var lightDeepPink: UIColor {
        if #available(iOS 11.0, *) {
            return UIColor(named: "light_deep_pink", in: Bundle(for: AmuseeColors.self), compatibleWith: nil)!
        } else {
            return #colorLiteral(red: 0.9176470588, green: 0.2980392157, blue: 0.7529411765, alpha: 1)
        }
    }
    
    /// A color object whose hex value is #D948DD and whose alpha value is 1.0.
    public class var steelPink: UIColor {
        if #available(iOS 11.0, *) {
            return UIColor(named: "steel_pink", in: Bundle(for: AmuseeColors.self), compatibleWith: nil)!
        } else {
            return #colorLiteral(red: 0.8509803922, green: 0.2823529412, blue: 0.8666666667, alpha: 1)
        }
    }

}
