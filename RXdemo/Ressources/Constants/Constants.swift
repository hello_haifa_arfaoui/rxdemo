//
//  Constants.swift
//  RXdemo
//
//  Created by Haifa Arfaoui on 9/15/20.
//  Copyright © 2020 Haifa Arfaoui. All rights reserved.
//

import Foundation
class Constants{
    static let home = "Home"
    static let search = "Search"
    static let topSongs = "Top Songs"
    static let podcasts = "Podcasts"
    static let movies = "Movies"
    static let favoriteArtist = "Favorite Artists"
}
