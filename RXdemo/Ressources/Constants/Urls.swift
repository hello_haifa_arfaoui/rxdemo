//
//  Urls.swift
//  RXdemo
//
//  Created by Haifa Arfaoui on 9/15/20.
//  Copyright © 2020 Haifa Arfaoui. All rights reserved.
//

import Foundation

class Urls {

    static let baseUrl = "https://itunes.apple.com/"
    static let searchUrl = Urls.baseUrl + "search"
}
