//
//  UIImageExtensions.swift
//  RXdemo
//
//  Created by Haifa Arfaoui on 9/15/20.
//  Copyright © 2020 Haifa Arfaoui. All rights reserved.
//

import Foundation
import UIKit

fileprivate class AmusseImage { }

extension UIImage {
    
    public class var icHome: UIImage {
        return UIImage(named: "ic_home", in: Bundle(for: AmusseImage.self), compatibleWith: nil)!
    }
    
    public class var icSearch: UIImage {
        return UIImage(named: "ic_search", in: Bundle(for: AmusseImage.self), compatibleWith: nil)!
    }
    
}

